/*-------------------------------------------------------------------------
 *
 * pg_matrix.c
 *	  pg_matrix extension
 *
 *-------------------------------------------------------------------------
 */
#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/array.h"
#include "utils/numeric.h"

//#include "pg_matrix.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(matrix_in);
PG_FUNCTION_INFO_V1(matrix_out);
PG_FUNCTION_INFO_V1(matrix_plus);
PG_FUNCTION_INFO_V1(matrix_multiply);
PG_FUNCTION_INFO_V1(array2matrix);

typedef struct matrix {
	int	n;
	int	values[FLEXIBLE_ARRAY_MEMBER];
} matrix;

static int
matrix_len(int n)
{
	return (offsetof(matrix, values) + (n*n) * sizeof(int));
}

Datum
matrix_in(PG_FUNCTION_ARGS)
{
	int		p, q, r, s;
	int		len;
	char   *a = PG_GETARG_CSTRING(0);
	char   *v;
	matrix *m;

	if (sscanf(a, "(%d %d %d %d)", &p, &q, &r, &s) != 4)
		elog(ERROR, "kabooom!");

	len = matrix_len(2);

	v = palloc(VARHDRSZ + len);
	SET_VARSIZE(v, VARHDRSZ + len);
	m = (matrix *) VARDATA(v);

	m->n = 2;
	m->values[0] = p;
	m->values[1] = q;
	m->values[2] = r;
	m->values[3] = s;

	PG_RETURN_POINTER(v); // proc v, proc ne m?
}

Datum
matrix_out(PG_FUNCTION_ARGS)
{
	char   *v = PG_GETARG_POINTER(0);
	matrix *m = (matrix *) VARDATA(v);
	char   *buff;

	buff = palloc(1024);

	sprintf(buff, "(%d %d %d %d)", m->values[0], m->values[1], m->values[2], m->values[3]);

	PG_RETURN_CSTRING(buff);
}

Datum
array2matrix(PG_FUNCTION_ARGS)
{
	//int		p, q, r, s;
	int		len;
	//char   *a = PG_GETARG_CSTRING(0);
	char   *v;
	matrix *m;

	ArrayType *vcid = PG_GETARG_ARRAYTYPE_P(0);
        int cid_dim = ARR_DIMS(vcid)[0];
        int *cid_data = (int *)ARR_DATA_PTR(vcid);


	//if (sscanf(a, "(%d %d %d %d)", &p, &q, &r, &s) != 4)
	//	elog(ERROR, "kabooom!");

	len = matrix_len(2);

	v = palloc(VARHDRSZ + len);
	SET_VARSIZE(v, VARHDRSZ + len);
	m = (matrix *) VARDATA(v);

	m->n = 2;

	for (int i = 0; i < cid_dim; i++)
		m->values[i] = cid_data[i];

	PG_RETURN_POINTER(v); // proc v, proc ne m?
}

Datum
matrix_plus(PG_FUNCTION_ARGS)
{
	char   *v1 = PG_GETARG_POINTER(0);
	char   *v2 = PG_GETARG_POINTER(1);
	char   *v3;

	matrix *m1 = (matrix *) VARDATA(v1);
	matrix *m2 = (matrix *) VARDATA(v2);
	matrix *m3;

	v3 = palloc(VARHDRSZ + matrix_len(2));
	SET_VARSIZE(v3, VARHDRSZ + matrix_len(2));
	m3 = (matrix *) VARDATA(v3);

	m3->values[0] = m1->values[0] + m2->values[0];
	m3->values[1] = m1->values[1] + m2->values[1];
	m3->values[2] = m1->values[2] + m2->values[2];
	m3->values[3] = m1->values[3] + m2->values[3];

	PG_RETURN_CSTRING(v3);
}

Datum
matrix_multiply(PG_FUNCTION_ARGS)
{
	char   *v1 = PG_GETARG_POINTER(0);
	char   *v2 = PG_GETARG_POINTER(1);
	char   *v3;

	matrix *m1 = (matrix *) VARDATA(v1);
	matrix *m2 = (matrix *) VARDATA(v2);
	matrix *m3;

	v3 = palloc(VARHDRSZ + matrix_len(2));
	SET_VARSIZE(v3, VARHDRSZ + matrix_len(2));
	m3 = (matrix *) VARDATA(v3);

	m3->values[0] = m1->values[0] * m2->values[0] + m1->values[1] * m2->values[2];
	m3->values[1] = m1->values[0] * m2->values[1] + m1->values[1] * m2->values[3];
	m3->values[2] = m1->values[2] * m2->values[0] + m1->values[3] * m2->values[2];
	m3->values[3] = m1->values[2] * m2->values[1] + m1->values[3] * m2->values[3];

	PG_RETURN_CSTRING(v3);
}
