# pg_matrix/Makefile
#

MODULE_big = pg_matrix

OBJS = pg_matrix.o

EXTENSION = pg_matrix
DATA = pg_matrix--1.0.sql
REGRESS = install_test \
	cast \
	operators

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

ifndef MAJORVERSION
    MAJORVERSION := $(basename $(VERSION))
endif
